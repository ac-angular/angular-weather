# AngularWeather

Aplicación del clima. Cliente en la arquitectura cliente-servidor.

## Conceptos a desarrollar

Cliente, Consumo de API, SVG interactivo,

## Conceptos específicos de Angular a desarrollar

Componentes, Comunicación entre componentes, Service, Interface

## Tecnologias

Lenguaje: Typescript

Framework: Angular (versión: 9.1.3)

## Uso

Para su uso ejecutar los siguientes comandos:

```bash
1) npm install
2) ng serve -o
```
