import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Province } from '../interfaces/province';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  data: string = environment.data;
  constructor(private httpClient: HttpClient) {}

  getData(): Observable<Province[]> {
    return this.httpClient.get<Province[]>(this.data, { responseType: 'json' });
  }
}
