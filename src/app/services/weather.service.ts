import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WeatherService {
  URI: string = environment.apiWeather;
  key: string = environment.keyWeather;

  constructor(private httpClient: HttpClient) {
    this.URI = `${this.URI}${this.key}`;
    console.log('environment.production', environment.production);
    
    if (environment.production) {
      this.URI += '&geocode=';
    }
  }

  getWeather(search: string) {
    if (environment.production) {
      return this.httpClient.get(`${this.URI}${search}`);      
    } else {
      return this.getWeatherTest();
    }
  }

  getWeatherTest() {
    console.log(this.URI);
    return new Observable(subscriber => {
      setTimeout(() => {
        this.httpClient.get(this.URI, {responseType: 'json'}).subscribe(data => {
          const length = Object.keys(data).length;        
          subscriber.next(data[Object.keys(data)[Math.floor(Math.random() * length)]]);
        })          
      }, 1000);
    });
  }
}
