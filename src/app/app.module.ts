import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MapComponent } from './components/map/map.component';
import { InfoComponent } from './components/info/info.component';
import { ListComponent } from './components/list/list.component';
import { registerLocaleData } from '@angular/common';
import localeAr from '@angular/common/locales/es-AR';
import localeArExtra from '@angular/common/locales/extra/es-AR';
import { SpinnerComponent } from './shared/spinner/spinner.component';

registerLocaleData(localeAr, 'es-AR', localeArExtra);

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    InfoComponent,
    ListComponent,
    SpinnerComponent,
  ],
  imports: [BrowserModule, HttpClientModule],
  providers: [{ provide: LOCALE_ID, useValue: 'es-AR' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
