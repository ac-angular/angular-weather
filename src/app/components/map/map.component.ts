import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Province } from 'src/app/interfaces/province';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  provinces: Province[] = [];
  @Input() provinceSelected: Province;
  @Input() searchEntered: string;
  @Output() mapClicked = new EventEmitter<Province>();
  @Output() mapEntered = new EventEmitter<string>();

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.getData().subscribe((provinces) => {
      this.provinces = provinces;
    });
  }

  selected(search, title) {
    const province: Province = {
      id: '',
      search,
      title,
      d: '',
    };
    this.mapClicked.emit(province);
  }
  mouseEnter(search: string): void {
    this.mapEntered.emit(search);
  }
}
