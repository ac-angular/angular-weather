import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Province } from 'src/app/interfaces/province';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  provinces: Province[] = [];
  @Input() provinceSelected: Province;
  @Input() searchEntered: string;
  @Output() listClicked = new EventEmitter<Province>();
  @Output() listEntered = new EventEmitter<string>();

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.getData().subscribe((provinces) => {
      this.provinces = provinces;
    });
  }
  selected(search, title): void {
    const province: Province = {
      id: '',
      search,
      title,
      d: '',
    };
    this.listClicked.emit(province);
  }
  mouseEnter(search: string): void {
    this.listEntered.emit(search);
  }
}
