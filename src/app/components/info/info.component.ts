import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import { Province } from 'src/app/interfaces/province';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit, OnChanges {
  @Input() provinceSelected: Province;
  info: any = {};
  err: any = {};
  iconurl = '';
  today: number = Date.now();
  constructor(private weatherService: WeatherService) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.provinceSelected) {
      this.getWeather();
    }
  }

  getWeather() {
    if (this.provinceSelected === undefined) {
      return;
    }
    this.err = undefined;
    this.info = undefined;
    const myOserver = {
      next: (data) => {
        this.info = data;
      },
      error: (err) => {
        switch (err.error.cod) {
          case '404':
            this.err = 'Ciudad no encontrada';
            break;
          default:
            this.err = 'Se produjo un error en la búsqueda';
            break;
        }
        console.log(err);
      },
    };
    const search = encodeURIComponent(this.provinceSelected.search.trim());
    this.weatherService.getWeather(search).subscribe(myOserver);
  }
}
