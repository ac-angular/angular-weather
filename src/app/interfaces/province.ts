export interface Province {
  id: string;
  title: string;
  search: string;
  d: string;
}
