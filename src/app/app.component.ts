import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import { Province } from './interfaces/province';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'angular-weather';
  provinceSelected: Province;
  searchEntered: string = '';
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.dataService.getData().subscribe((provinces) => {
      this.provinceSelected = provinces.find(
        (province) => province.title === 'San Luis'
      );
    });
  }

  childMapClicked(province: Province) {
    this.provinceSelected = province;
  }

  childMapEntered(search: string) {
    this.searchEntered = search;
  }

  childListClicked(province: Province) {
    this.provinceSelected = province;
  }

  childListEntered(search: string) {
    this.searchEntered = search;
  }
}
